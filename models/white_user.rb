class WhiteUser < ActiveRecord::Base
  belongs_to :group

  def load_name
    driver = VkApi.new self.group.select_bot
    data = driver.run_method 'users.get', user_ids: self.vk_id
    self.name = "#{data['response'][0]['first_name']} #{data['response'][0]['last_name']}"
  end
end