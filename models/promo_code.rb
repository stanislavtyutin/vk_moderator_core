class PromoCode < ActiveRecord::Base
  belongs_to :promo_code_campaign
  belongs_to :group

  def set_user_vk_id(user_vk_id)
    self.user_vk_id = user_vk_id
    self.save if self.changed?
  end

  def set_group(new_group)
    unless self.group
      self.group = new_group
      self.save
    end
  end

  def compile_message
    promo_code_campaign.template.gsub('[promo_code]', self.code)
  end

end