class Attachment < ActiveRecord::Base
  belongs_to :subject, polymorphic: true
  serialize :full_data, Hash

  def fill_data
    case self.attachment_type
      when 'photo'
        self.data = self.full_data['photo']['photo_1280'] ||
            self.full_data['photo']['photo_604'] ||
            self.full_data['photo']['photo_130'] ||
            self.full_data['photo']['photo_75']
      when 'link'
        self.data = self.full_data['link']['url']
      when 'doc'
        if %w(jpg png).include? self.full_data['doc']['ext']
          self.data = self.full_data['doc']['url']
        end
      else
        nil
    end
  end

  def get_string
    if self.attachment_type == 'link'
      self.data
    else
      "#{self.attachment_type}#{self.full_data[self.attachment_type]['owner_id']}_#{self.full_data[self.attachment_type]['id']}"
    end
  end

end