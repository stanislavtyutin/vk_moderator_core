class Bot < ActiveRecord::Base
  has_and_belongs_to_many :groups

  def load_groups
    driver = VkApi.new self
    data = driver.run_method 'groups.get', user_id: self.vk_id, count: 1000, filter: 'moder'
    group_vk_ids = data['response']['items']

    group_vk_ids.each do |vk_id|
      group = Group.find_by_vk_id vk_id

      if group
        if group.name.nil? || group.name.empty?
          group_data = driver.run_method 'groups.getById', group_ids: vk_id
          group.name = group_data['response'][0]['name']
          group.save
        end
      else
        group_data = driver.run_method 'groups.getById', group_ids: vk_id
        group = Group.create vk_id: vk_id, name: group_data['response'][0]['name']
      end

      unless group.bots.include? self
        group.bots << self
      end
    end
  end

  def load_name
    driver = VkApi.new self
    data = driver.run_method 'users.get', user_ids: self.vk_id
    self.name = "#{data['response'][0]['first_name']} #{data['response'][0]['last_name']}"
  end

end

# url = "#{vk_app[:auth_url]}?client_id=#{vk_app[:client_id]}&redirect_uri=#{vk_app[:redirect_uri]}&display=page&scope=#{vk_app[:scope]}&response_type=token&v=#{vk_app[:v]}"

# https://oauth.vk.com/authorize?client_id=5701826&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=335872&response_type=token&v=5.59

# access = 77e32f843e7cff239a34b863579340cac9c9c88d0a50ea5ad97ad80ff40b08a2b8fc61b59f8fc3e66d09155740d68