class Post < ActiveRecord::Base
  belongs_to :group
  has_many :attachments, as: :subject
  has_many :comments, as: :subject
  has_one :timetable_item
  has_one :post_request

  include AntiSpam
  include ScheduledPublication

  def group_vk_id
    self.group.vk_id
  end

  def publish_suggested(time = nil, signed = false)
    return unless self.post_type == 'suggest'

    driver = VkApi.new self.group.select_bot

    options = {owner_id: "-#{self.group.vk_id}", post_id: self.vk_id, signed: signed ? 1 : 0}
    options[:attachments] = reload_images.join(',') if self.attachments.any?
    options[:publish_date] = time.to_i if time
    driver.run_method 'wall.post', options
  end

  def publish_from_group(time = nil)
    driver = VkApi.new self.group.select_bot
    options = {owner_id: "-#{self.group.vk_id}", from_group: 1, message: text_with_link}
    options[:publish_date] = time.to_i if time
    # options[:attachments] = self.attachments.map { |t| t.get_string }.join(',') if self.attachments.any?
    options[:attachments] = reload_images.join(',') if self.attachments.any?
    driver.post_method 'wall.post', options
  end

  private

  def text_with_link
    driver = VkApi.new self.group.select_bot
    data = driver.run_method 'users.get', user_ids: self.from_vk_id
    name = "#{data['response'][0]['first_name']} #{data['response'][0]['last_name']}"
    "[id#{self.from_vk_id}|#{name}]\n#{self.text}"
  end

  def reload_images
    random = Random.new
    tmp_dir = "/tmp/vkm/posts/#{self.id}-#{random.hex 8}"
    RakeFileUtils.mkpath tmp_dir

    self.attachments.all.each do |attach|
      if attach.attachment_type == 'photo' ||
          (attach.attachment_type == 'doc' && %w(jpg png).include?(attach.full_data['doc']['ext']))
        File.open(File.join(tmp_dir, attach.id.to_s), 'wb') do |f|
          open(attach.data, 'rb') do |r|
            f.write(r.read)
          end
        end
      end
    end

    require 'rmagick'

    Dir["#{tmp_dir}/*"].each do |filename|
      image = Magick::Image.read(filename).first
      # noinspection RubyArgCount
      mark = Magick::Image.new(image.columns, image.rows) { self.background_color = "none" }

      gc = Magick::Draw.new
      gc.gravity = Magick::SouthEastGravity
      gc.pointsize = 32
      gc.font_family = "Arial"
      gc.font_weight = Magick::BoldWeight
      gc.stroke = 'none'
      gc.annotate mark, 0, 0, 0, 0, self.group.name

      image = image.watermark mark, 0.25

      image.write filename
    end

    driver = VkApi.new self.group.select_bot

    upload_server = driver.run_method('photos.getWallUploadServer', group_id: self.group.vk_id)['response']['upload_url']

    new_images = []

    Dir["#{tmp_dir}/*"].each do |filename|
      new_images << upload_file(filename, upload_server)
    end

    new_images.compact
  ensure
    FileUtils.rm_r(tmp_dir) if File.exists? tmp_dir
  end

  def upload_file(filename, server)
    require 'net/http'

    uri = URI server
    connection = Net::HTTP.new uri.host, uri.port
    connection.use_ssl = true
    connection.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new uri

    form_data = [
        ['photo', File.open(filename), {filename: filename.split('/').last + '.jpg'}]
    ]

    request.set_form form_data, 'multipart/form-data'

    response = connection.request request

    data = JSON.parse response.body

    driver = VkApi.new self.group.select_bot
    response = driver.run_method 'photos.saveWallPhoto', group_id: self.group.vk_id,
                                 photo: data['photo'], server: data['server'], hash: data['hash']

    owner_id = response['response'].first['owner_id']
    photo_id = response['response'].first['id']
    "photo#{owner_id}_#{photo_id}"
  rescue
    nil
  end

end