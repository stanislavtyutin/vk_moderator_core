module AntiSpam
  include SpamDetector
  include RegularRemover

  def from_group?
    self.from_vk_id == -self.group.vk_id
  end

  def anti_spam
    if from_group?
      regular_remove
    else
      puts "AntiSpam for #{self.class}"
      result = check
      if result
        regular_remove
      else
        if self.respond_to? :check_result=
          self.check_result = false
          self.save
        end
        destroy_object
      end
    end
  end

  def check
    if self.group.white_users.where('start_date < ?', self.publish_date)
           .where('stop_date > ?', self.publish_date)
           .where(vk_id: self.from_vk_id).any?

      true
    else
      (!self.group.enable_check_text_exists || check_text_exists) &&
          (!self.group.enable_check_timeout || self.kind_of?(Comment) || check_timeout) &&
          (!self.group.enable_check_stop_words || check_stop_words) &&
          (!self.group.enable_check_links || check_links) &&
          (!self.group.enable_check_membership || check_membership)
    end
  end

  def get_removing_reason
    reasons = {}

    reasons[:in_white_list] = self.group.white_users.where('start_date < ?', self.publish_date)
                                  .where('stop_date > ?', self.publish_date)
                                  .where(vk_id: self.from_vk_id).any?

    if self.group.enable_check_text_exists
      reasons[:text_exists] = check_text_exists
    end

    if self.group.enable_check_timeout && !self.kind_of?(Comment)
      reasons[:timeout] = check_timeout
    end

    if self.group.enable_check_stop_words
      reasons[:stop_words] = check_stop_words
      reasons[:first_stop_word] = get_stop_word
    end

    if self.group.enable_check_links
      reasons[:links] = check_links
    end

    if self.group.enable_check_membership
      reasons[:membership] = check_membership
    end

    reasons
  end

  def destroy_object
    raise AlreadyDeleted if self.is_deleted

    case self
      when Post
        vk_api.run_method 'wall.delete', owner_id: "-#{self.group_vk_id}", post_id: self.vk_id
        mark_as_deleted
        return nil
      when Comment
        destroy_comment
        return nil
      else
        nil
    end
  end

  def select_bot
    case self
      when Post
        return self.group.bots.all.sample
      when Comment
        return self.subject.select_bot
      else
        raise NoBotError
    end
  end

  private

  def destroy_comment
    case self.subject
      when Post
        vk_api.run_method 'wall.deleteComment', owner_id: "-#{self.group_vk_id}", comment_id: self.vk_id
        mark_as_deleted
      when Topic
        vk_api.run_method 'board.deleteComment', group_id: self.group_vk_id, topic_id: self.subject.vk_id,
                          comment_id: self.vk_id
        mark_as_deleted
      else
        nil
    end
  end

  def vk_api
    unless @vk_api
      @vk_api = VkApi.new select_bot
    end
  end

  def mark_as_deleted
    self.is_deleted = true
    self.save
  end


end