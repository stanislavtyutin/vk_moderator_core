class PostRequest < ActiveRecord::Base
  belongs_to :post
  serialize :data, Hash

  scope :active, -> { where(active: true) }
  scope :posted, -> { where(posted: true) }

  def self.make(post)
    return unless post.group.city

    return unless post.check_result

    return if PostRequest.find_by_user_and_group(post.from_vk_id, post.group).active.any?

    request = PostRequest.new post: post

    unless PostRequest.find_by_user_and_group(post.from_vk_id, post.group).posted.any?
      request.allow_request_type(:repost) if request.friends_count >= GlobalConfig.min_followers
    end

    if request.friends_count >= GlobalConfig.min_followers
      invite_list = request.friends_for_invite
      if invite_list.size == GlobalConfig.request_invite_count
        request.allow_request_type :invite
        request.data = {invited_vk_ids: invite_list}
      end
    end

    if request.allowed?
      request.save
      zonbibare_api = ZonbibareApi.new
      zonbibare_api.create_send_message_task request.post.from_vk_id, request.messages
    end

    nil
  end

  def self.find_by_user_and_group(user_vk_id, group)
    PostRequest.joins(:post).where('from_vk_id = ?', user_vk_id).where('group_id = ?', group.id)
  end

  def self.process
    PostRequest.where(active: true).all.each do |post_request|
      if post_request.check_conditions
        timetable_item = post_request.post.group.get_nearest_timetable_item
        if timetable_item
          timetable_item.post = post_request.post
          timetable_item.publish_date = timetable_item.get_publish_time
          post_request.post.publish_from_group timetable_item.publish_date
          timetable_item.save
          post_request.posted = true
          post_request.active = false
          post_request.save
        end
      else
        if post_request.created_at < Time.now - 24.hours
          post_request.active = false
          post_request.save
        end
      end
    end
  end

  def allow_request_type(type)
    if self.request_type.nil?
      self.request_type = type.to_s
    else
      self.request_type += "&#{type.to_s}"
    end
  end

  def allowed?
    !!self.request_type
  end

  # def followers_count
  #   driver = VkApi.new post.group.select_bot
  #   data = driver.run_method 'users.get', user_ids: post.from_vk_id, fields: 'followers_count'
  #   data['response'][0]['followers_count'] #TODO: Error here
  # end

  def friends_count
    driver = VkApi.new post.group.select_bot
    data = driver.run_method 'friends.get', user_id: post.from_vk_id, fields: 'city'
    total = data['response']['count']
    in_city = data['response']['items'].select { |t| t['city'] == self.post.group.city }.count

    if in_city.to_f / total.to_f > 0.3
      total
    else
      0
    end
  end

  def friends_for_invite
    driver = VkApi.new post.group.select_bot
    data = driver.run_method 'friends.get', user_id: post.from_vk_id
    user_list = data['response']['items']
    member_list = []
    until user_list.empty?
      data = driver.post_method 'groups.isMember', group_id: self.post.group.vk_id, extended: 1,
                                user_ids: user_list.pop(rand(500))
      member_list += data['response']
    end

    member_list.select { |t| t['member'] != 1 && t['request'] != 1 && t['invitation'] != 1 }
        .map { |t| t['user_id'] }
  end

  def messages
    MessagesTemplate.process self.post.group.post_request_messages_template, self.post, self.request_type.split('&')
  end

  def invited_friends_count
    driver = VkApi.new post.group.select_bot
    ids = self.data[:invited_vk_ids]
    list = []
    until list.empty?
      data = driver.post_method 'groups.isMember', group_id: self.post.group.vk_id, extended: 1,
                                user_ids: ids.pop(rand(500)).join(',')
      list += data['response']
    end

    list.select { |t| t['member'] == 1 || t['request'] == 1 || t['invitation'] == 1 }.count
  end

  def has_repost?
    RepostChecker.check self.post.group, self.post.from_vk_id
  end

  def friends_invited?
    invited_friends_count >= GlobalConfig.min_invited_friends
  end

  def check_conditions
    (self.request_type.include?('repost') && has_repost?) ||
        (self.request_type.include?('invite') && friends_invited?)
  end

end