class Topic < ActiveRecord::Base
  belongs_to :group
  has_many :comments, as: :subject

  def group_vk_id
    self.group.vk_id
  end

  def select_bot
    self.group.select_bot
  end

end