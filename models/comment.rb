class Comment < ActiveRecord::Base
  belongs_to :subject, polymorphic: true
  has_many :attachments, as: :subject

  include AntiSpam

  def group_vk_id
    self.subject.group_vk_id
  end

  def group
    self.subject.group
  end

end