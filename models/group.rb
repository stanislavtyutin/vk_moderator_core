class Group < ActiveRecord::Base
  has_many :posts
  has_many :topics
  has_many :white_links
  has_many :white_users
  has_many :timetable_items
  has_and_belongs_to_many :bots
  belongs_to :dialog_structure
  has_many :dialogs

  def load_topics
    driver = VkApi.new select_bot
    data = driver.run_method 'board.getTopics', group_id: self.vk_id
    topic_data = data['response']['items']
    topic_data.each do |t|
      topic = self.topics.where(vk_id: t['id']).first || Topic.new(group: self, vk_id: t['id'])
      topic.title = t['title']
      topic.publish_date = Time.at t['created']
      topic.is_closed = t['is_closed'] == 1
      topic.is_fixed = t['is_fixed'] == 1
      topic.save
    end
    self.topics
  end

  def select_bot
    self.bots.all.sample
  end

  def get_stop_words
    GlobalConfig.stop_words + self.stop_words.split(',').map { |t| t.strip }
  end

  def get_timeout
    self.post_timeout || GlobalConfig.post_timeout
  end

  def get_old_timeout
    self.old_timeout || GlobalConfig.old_timeout
  end

  def get_max_timetable_offset
    self.max_timetable_offset || GlobalConfig.max_timetable_offset
  end

  def is_member?(vk_id)
    redis = RedisProvider.instance.redis
    member_pattern = "member-#{self.vk_id}-#{vk_id}"
    if redis.get(member_pattern) == true.to_s
      true
    else
      driver = VkApi.new select_bot
      data = driver.run_method 'groups.isMember', group_id: self.vk_id, user_id: vk_id
      result = data['response'] == 1
      if result
        redis.set member_pattern, true.to_s
        return true
      end
      false
    end
  end

  def get_nearest_timetable_item
    items = self.timetable_items.all.select { |t| t.free? }
    item = items.sort { |x, y| x.get_publish_time <=> y.get_publish_time }.first
    if item && (item.get_publish_time - Time.now) < get_max_timetable_offset.hours
      item
    else
      nil
    end
  end

  def send_message(user_vk_id, message)
    driver = VkApi.new self
    driver.post_method 'messages.send', user_id: user_vk_id, message: message
    true
  rescue
    false
  end

  def get_dialog(user_vk_id)
    driver = VkApi.new self
    data = driver.run_method 'messages.getHistory', user_id: user_vk_id, count: 200, rev: 1
    data['response']['items'].map do |t|
      {incoming: t['out'] == 0,
       message: t['body'],
       publish_date: Time.at(t['date'])
      }
    end
  end

  def mark_message_as_read(message_vk_id)
    driver = VkApi.new self
    driver.run_method 'messages.markAsRead', message_ids: message_vk_id
    nil
  end

  def get_invites(offset = 0)
    driver = VkApi.new select_bot
    data = driver.run_method 'groups.getInvitedUsers', group_id: self.vk_id, count: 1000, offset: offset
    data['response']['items'].map { |t| t['id'] }
  end

  def get_members_count
    driver = VkApi.new select_bot
    data = driver.run_method 'groups.getById', group_id: self.vk_id, fields: 'members_count'
    data['response'][0]['members_count']
  end

  def process_message(data)

    user_vk_id = data['user_id']
    text = data['body']

    DialogPlugins.constants.select { |t| DialogPlugins.const_get(t).is_a? Class }
        .map { |t| DialogPlugins.const_get(t) }.each do |dialog_plugin|
      if dialog_plugin.respond_to_key? text, self
        response = dialog_plugin.get_reply text, user_vk_id, self
        if response
          send_message user_vk_id, response
          return
        end
      end
    end

    return unless self.dialog_structure

    dialog = self.dialogs.find_by_user_vk_id user_vk_id
    if dialog
      new_node = dialog.dialog_structure.children.find_by_key text.mb_chars.upcase.to_s

      unless new_node
        if (dialog.dialog_structure.back_key == text.mb_chars.upcase.to_s) && (dialog.dialog_structure.parent)
          new_node = dialog.dialog_structure.parent
        elsif dialog.dialog_structure.reset_key == text.mb_chars.upcase.to_s
          new_node = self.dialog_structure
        end
      end

      if new_node
        send_message user_vk_id, new_node.text
        dialog.dialog_structure = new_node
        dialog.save
      else
        mark_message_as_read data['id']
      end
    else
      dialog = Dialog.create(group: self, dialog_structure: self.dialog_structure, user_vk_id: user_vk_id)
      send_message user_vk_id, dialog.dialog_structure.text
    end

  end

end