class DialogStructure < ActiveRecord::Base
  has_one :group
  has_many :dialogs

  belongs_to :parent, class_name: 'DialogStructure'
  has_many :children, foreign_key: :parent_id, class_name: 'DialogStructure'
end