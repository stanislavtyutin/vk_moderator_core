class PromoCodeCampaign < ActiveRecord::Base
  has_many :promo_codes
  default_scope { where(active: true) }

  def self.get_code(keys, user_vk_id, group)
    campaign = PromoCodeCampaign.find_by_key find_key(keys)
    campaign&.get_code user_vk_id, group
  end

  def self.has_campaign?(keys)
    not find_key(keys).nil?
  end

  def self.find_key(keys)
    all_keys = PromoCodeCampaign.pluck(:key).map { |t| t.split(',').map { |t1| t1.strip } }.flatten
    result = keys & all_keys
    result.first
  end

  def self.find_by_key(key)
    PromoCodeCampaign.pluck(:id, :key).each do |t|
      return PromoCodeCampaign.find(t[0]) if t[1].include?(key)
    end
    nil
  end

  def get_code(user_vk_id, group)
    if check(user_vk_id, group)
      puts 'ok'
      promo_code = self.promo_codes.find_by(user_vk_id: user_vk_id) || self.promo_codes.where(user_vk_id: nil).first
      promo_code&.set_user_vk_id user_vk_id
      promo_code&.set_group group
      promo_code&.compile_message
    else
      self.min_friends_fail_message
    end
  end

  def check(user_vk_id, group)
    return true if self.min_friends_count == 0

    driver = VkApi.new group.select_bot
    data = driver.run_method 'friends.get', user_id: user_vk_id, fields: 'city'
    total = data['response']['count']
    total > self.min_friends_count
  end

end