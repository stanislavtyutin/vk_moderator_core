module UrlChecker
  require 'uri'

  def check_url(url)
    if is_real_link?(url)
      in_white_list? url
    else
      true
    end
  end

  def is_real_link?(url)
    URI.parse(URI.encode(url))
    has_ip? parse_url(url)[0]
  rescue
    return false
  end

  def parse_url(url)
    if /https?:\/\//.match url
      elements = url.split('/') # ["https:", "", "yandex.ru", "test", "abcd", "regex.html"]
      domain = elements[2]
      sitename = elements[2..-1].join
    else
      elements = url.split('/') # ["yandex.ru", "test", "abcd", "regex.html"]
      domain = elements[0]
      sitename = elements.join
    end
    [domain, sitename]
  end

  def has_ip?(hostname)
    Resolv.getaddress hostname
    true
  rescue Resolv::ResolvError, ArgumentError
    return false
  end

  def in_white_list?(url)
    white_links = self.group.white_links.where('start_date < ?', self.publish_date)
                      .where('stop_date > ?', self.publish_date).pluck(:link)

    white_links.each do |link|
      return true if parse_url(link) == parse_url(url)
    end

    false
  end

end