ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)
require 'bundler/setup'
Bundler.require

require 'digest'
require 'active_support/concern'
require 'net/http'
require 'open-uri'

require_all 'lib/**/*.rb'
require_all 'models/**/*.rb'
