class VkApi
  require 'yaml'
  require 'uri'
  require 'net/http'

  CONFIG_FILE = 'config/vk.yml'.freeze

  def self.app_install_link
    config = YAML.load(File.read(CONFIG_FILE))
    "#{config['auth_url']}?client_id=#{config['client_id']}&display=page&redirect_uri=#{config['redirect_url']}&scope=#{config['scope']}&response_type=token&v=#{config['api_version']}"
  end

  def self.app_group_install_link(group_vk_id)
    config = YAML.load(File.read(CONFIG_FILE))
    "#{config['auth_url']}?client_id=#{config['client_id']}&group_ids=#{group_vk_id}&display=page&redirect_uri=#{config['redirect_url']}&scope=#{config['group_scope']}&response_type=token&v=#{config['api_version']}"
  end

  def initialize(source)
    @source = source
  end

  def run_method(method_name, params = nil)
    uri = get_method_uri method_name, params

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Get.new(uri.request_uri)
    request['Accept-Language'] = 'ru'
    response = http.request(request)

    if response.code.to_s == 200.to_s
      data = JSON.parse response.body
      if data['error']
        case data['error']['error_code']
          when 3, 8, 100
            raise InvalidMethodError.new(data['error']['error_msg'])
          when 2, 4, 5, 7, 8, 11, 14, 15, 16, 17, 18, 20, 23, 24, 101, 113, 150, 200, 201, 203, 500, 900, 901, 902
            raise AccessDeniedError.new(data['error']['error_msg'])
          when 6
            sleep 2
            return run_method(method_name, params)
          else
            raise VkApiError.new(data['error']['error_msg'])
        end
      else
        return data
      end
    end
  end

  def post_method(method_name, params = nil)
    uri = get_method_uri method_name

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new(uri.request_uri)
    request['Accept-Language'] = 'ru'
    request.body = params.map { |k, v| "#{k}=#{v}" }.join('&')
    response = http.request(request)

    if response.code.to_s == 200.to_s
      data = JSON.parse response.body
      if data['error']
        case data['error']['error_code']
          when 3, 8, 100
            raise InvalidMethodError.new(data['error']['error_msg'])
          when 2, 4, 5, 7, 8, 11, 14, 15, 16, 17, 18, 20, 23, 24, 101, 113, 150, 200, 201, 203, 500
            raise AccessDeniedError.new(data['error']['error_msg'])
          when 6
            sleep 2
            return post_method(method_name, params)
          else
            raise VkApiError.new(data['error']['error_msg'])
        end
      else
        return data
      end
    end
  end

  private

  def config
    unless @config
      @config = YAML.load(File.read(CONFIG_FILE))
    end
    @config
  end

  def get_method_uri(method_name, params = nil)
    params_str = ''
    params&.each do |key, value|
      params_str+="&#{key}=#{value}"
    end

    access_token = @source.access_token

    URI("#{config['api_url']}/method/#{method_name}?access_token=#{access_token}&v=#{config['api_version']}&lang=ru#{params_str}")
  end

end