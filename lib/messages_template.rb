class MessagesTemplate

  def self.process(template, post, conditions = [])
    template = template.clone

    template.gsub! /\[[^\[\]]+\]/ do |tag|
      case tag
        when '[group_name]'
          post.group.name
        when '[group_count]'
          post.group.get_members_count
        else
          tag
      end
    end

    template.gsub! /\[if\(\w+\)\][^\[\]]+\[endif\]/ do |str|
      cond = str.scan(/\[if\(\w+\)\]/).first[4..-3]
      if conditions.include? cond
        str.gsub /\[[^\[\]]+\]/, ''
      else
        nil
      end
    end

    template.split '[next_message]'
  end

end