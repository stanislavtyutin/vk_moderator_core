class RedisProvider
  include Singleton

  REDIS_CONFIG_PATH = 'config/redis.yml'.freeze

  def initialize
    config = YAML.load(File.read(REDIS_CONFIG_PATH))
    @redis = Redis.new host: config['host'], port: config['port'], db: config['db']
  end

  def redis
    @redis
  end

end