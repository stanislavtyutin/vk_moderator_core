class Session

  SESSION_TIMEOUT = 1.hours.freeze

  attr_accessor :instance
  attr_accessor :session_aes
  attr_accessor :token
  attr_accessor :timeout
  attr_accessor :created_at

  def initialize(instance)
    @instance = instance
    @token = SecureRandom.uuid
    @created_at = Time.now
    @timeout = SESSION_TIMEOUT
  end

  def update
    @created_at = Time.now
  end

  def expired?
    expires < Time.now
  end

  def expires
    @created_at + @timeout
  end

  def marshal_dump
    [@instance.id, @session_aes, @token, @created_at, @timeout]
  end

  def marshal_load(array)
    @instance_id, @session_aes, @token, @created_at, @timeout = array
    @instance = Instance.find @instance_id
  end

  def as_json(*)
    {
        token: token,
        created_at: created_at,
        timeout: timeout
    }
  end

end