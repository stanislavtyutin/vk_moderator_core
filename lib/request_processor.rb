class RequestProcessor

  def self.process(data)
    puts data['type']
    case data['type']
      when 'wall_post_new'
        wall_post_new data
      when 'wall_reply_new', 'wall_reply_edit'
        wall_reply_new data
      when 'board_post_new', 'board_post_edit', 'board_post_restore'
        board_post_new data
      when 'board_post_delete'
        board_post_delete data
      when 'message_new'
        message_new data
      else
        nil
    end
  rescue Exception => e
    puts e
    puts e.backtrace
  end

  private

  def self.wall_post_new(data)
    group = Group.find_by_vk_id data['group_id']
    if group&.active
      object = data['object']
      post = Post.create group: group, vk_id: object['id'], from_vk_id: object['from_id'],
                         publish_date: Time.at(object['date']), post_type: object['post_type'],
                         text: object['text']
      object['attachments']&.each do |attach_data|
        attach = Attachment.new subject: post, attachment_type: attach_data['type'], full_data: attach_data
        attach.fill_data
        attach.save
      end
      post.anti_spam if group.enable_check_wall
      post.publish_in_future if group.enable_auto_publish_suggested
      PostRequest.make(post) if group.enable_suggest_publish_from_group
    end
  end

  def self.wall_reply_new(data)
    group = Group.find_by_vk_id data['group_id']
    post = group&.posts.where(vk_id: data['object']&.[]('post_id')).first
    if group&.active && post
      object = data['object']
      comment = post.comments.where(vk_id: object['id']).first || Comment.new(subject: post, vk_id: object['id'])
      comment.from_vk_id = object['from_id']
      comment.publish_date = Time.at(object['date'])
      comment.text = object['text']
      comment.save

      comment.attachments.destroy_all

      object['attachments']&.each do |attach_data|
        attach = Attachment.new subject: comment, attachment_type: attach_data['type'], full_data: attach_data
        attach.fill_data
        attach.save
      end
      comment.anti_spam if group.enable_check_wall
    end
  end

  def self.board_post_new(data)
    group = Group.find_by_vk_id data['group_id']
    topic = group&.topics&.where(vk_id: data['object']&.[]('topic_id'))&.first
    if group&.active && topic
      object = data['object']
      comment = topic.comments.where(vk_id: object['id']).first || Comment.new(subject: topic, vk_id: object['id'])
      comment.from_vk_id = object['from_id']
      comment.publish_date = Time.at(object['date'])
      comment.text = object['text']
      comment.is_deleted = false
      comment.save

      object['attachments']&.each do |attach_data|
        attach = Attachment.new subject: comment, attachment_type: attach_data['type'], full_data: attach_data
        attach.fill_data
        attach.save
      end

      comment.anti_spam if group.enable_check_topics
    end
  end

  def self.board_post_delete(data)
    group = Group.find_by_vk_id data['group_id']
    topic = group&.topics&.where(vk_id: data['object']&.[]('topic_id'))&.first
    comment = topic&.comments&.where(vk_id: data['object']&.[]('id'))&.first
    if group&.active && comment
      comment.is_deleted = true
      comment.save
    end
  end

  def self.message_new(data)
    group = Group.find_by_vk_id data['group_id']
    if group&.active && group&.enable_auto_answering
      group.process_message data['object']
    end
  end

end