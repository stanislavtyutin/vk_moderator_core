module SpamDetector
  include LanguageNormalizer
  include UrlChecker

  def check_stop_words
    get_stop_word.nil?
  end

  def get_stop_word
    stop_words_list = self.group.get_stop_words
    source = normalize_language(self.text.mb_chars.upcase.to_s)

    stop_words_list.each do |word|
      return word if source.include? word.mb_chars.upcase.to_s
    end

    nil
  end

  def check_links
    check_url_links && check_vk_links
  end

  def check_url_links
    urls = self.text.scan(/\w{1,63}\.\w{2,10}\/?\S*/) +
        self.text.scan(/https?:\/\/\w{1,63}.\w{2,10}\/?\S*/) +
        self.attachments.where(attachment_type: 'link').all.map { |t| t.data }
    pass = true
    urls.each do |t|
      pass &&= check_url(t)
      break unless pass
    end
    pass
  end

  def check_vk_links
    self.text.scan(/\[\w+\|\S+\]/).empty?
  end

  def check_text_exists
    not self.text.empty?
  end

  def check_membership
    self.group.is_member? self.from_vk_id
  end

  def check_timeout
    user_vk_id = self.from_vk_id

    if self.kind_of? Post
      source = self.group.posts
      timeout = self.group.get_timeout.hours
      # elsif self.kind_of? Comment
      #   source = self.subject.comments
      #   timeout = nil
    else
      source = nil
      timeout = nil
    end

    if source
      not source.where.not(id: self.id).where(check_result: true).where(from_vk_id: user_vk_id)
              .where('publish_date > ?', self.publish_date - timeout).any?
    else
      true
    end
  end

end