class ZonbibareApi

  ZONBIBARE_API_CONFIG_PATH = 'config/zonbibare.yml'

  def create_send_message_task(user_vk_id, messages)
    data = {vk_id: user_vk_id, messages: messages, stop_word: 'пока'}
    create_job 'simple_dialog', data
  end

  private

  def create_job(action, data)
    job_data = {pool_id: config['pool_id'], action: action, data: data}
    uri = URI("http://#{config['host']}/jobs")
    put_request uri, job_data.to_json
  end

  def config
    @config ||= YAML.load(File.read(ZONBIBARE_API_CONFIG_PATH))
  end

  def put_request(uri, data)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Put.new(uri.request_uri)
    request.body = data
    http.request(request)
  end

end