class OldCleaner

  def self.clean
    Group.where('enable_remove_old_posts = ? or enable_remove_old_topic_comments = ?', true, true).all.each do |group|
      timeout = Time.now - group.get_old_timeout.days

      if group.enable_remove_old_posts
        group.posts.where(is_deleted: false).where('publish_date < ?', timeout).each do |post|
          begin
            post.destroy_object
          rescue
            post.is_deleted = true
            post.save
          end
        end
      end

      if group.enable_remove_old_topic_comments
        group.topics.all.each do |topic|
          topic.comments.where(is_deleted: false).where('publish_date < ?', timeout).each do |comment|
            begin
              comment.destroy_object
            rescue
              comment.is_deleted = true
              comment.save
            end
          end
        end
      end
    end
  end

end