module DialogPlugins

  class PromoCodePlugin

    def self.respond_to_key?(key, *)
      PromoCodeCampaign.has_campaign? keys_array(key)
    end

    def self.get_reply(key, vk_user_id, group, *)
      PromoCodeCampaign.get_code keys_array(key), vk_user_id, group
    end

    private

    def self.keys_array(key)
      key.gsub(/['"]/, '').gsub(/[(),.?!-]/, ' ').mb_chars.upcase.to_s.split(' ').compact
    end

  end

end