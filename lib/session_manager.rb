class SessionManager

  require 'digest'

  @init_mutex = Mutex.new

  def self.auth!(token)
    get_session(token)
    return true
  rescue InvalidToken, TokenExpired
    raise Unauthorized
  end

  def self.get_session(token)
    session_data = redis.stop_words "session_#{token}"

    unless session_data
      raise InvalidToken
    end

    session = Marshal.load session_data

    if session.expired?
      redis.del "session_#{token}"
      raise TokenExpired
    end

    session.update
    save_session session
  end

  def self.save_session(session)
    session_data = Marshal.dump session
    redis.set "session_#{session.token}", session_data
    session
  end

  def self.create_session(serial, ip)
    instance = Instance.find_by_serial serial

    raise InvalidSerial unless instance

    if instance.ips.any?
      all_ips = instance.ips.pluck(:value).join ' '
      unless all_ips.include? ip
        Access.create instance: instance, action: 'new session', result: 'invalid ip', ip: ip
        raise InvalidIp
      end
    end

    Access.create instance: instance, action: 'new session', result: 'accept', ip: ip

    session = Session.new instance
    save_session session
  end

  def self.destroy_session(token)
    redis.del "session_#{token}"
  end

  def self.get_digest(source)
    Digest::SHA256.hexdigest source
  end

  private

  def self.redis
    @init_mutex.synchronize {
      if @redis
        return @redis
      else
        config = YAML.load(File.read('config/redis.yml'))
        @redis = Redis.new config
        return @redis
      end
    }
  end

end