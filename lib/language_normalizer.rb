module LanguageNormalizer

  def normalize_language(str)
    words = str.split(' ')
    words.map { |t| normalize_word t }.join(' ')
  end

  private

  def normalize_word(word)
    russian = ('А'..'Я')
    english = ('A'..'Z')
    numeric = ('0'..'9')

    pass = true
    word.chars.each do |t|
      unless (russian.to_a + numeric.to_a).include? t
        pass = false
        break
      end
    end

    unless pass
      pass = true
      word.chars.each do |t|
        unless (english.to_a + numeric.to_a).include? t
          pass = false
          break
        end
      end
    end

    return word if pass

    new_word = ''
    if word.chars.count { |t| russian.include? t } > 0
      word.chars.each do |t|
        if russian.include? t
          new_word += t
        else
          new_word += get_same_char t
        end
      end
    end

    new_word
  end

  def get_same_char(char)
    case char
      when 'A'
        'А'
      when 'B'
        'В'
      when 'C'
        'С'
      when 'E'
        'Е'
      when 'H'
        'Н'
      when 'K'
        'К'
      when 'M'
        'М'
      when 'O'
        'О'
      when 'P'
        'Р'
      when 'T'
        'Т'
      when 'U'
        'И'
      when 'X'
        'Х'
      else
        char
    end
  end

end