module RegularRemover

  def regular_remove
    return unless self.kind_of? Post

    return unless self.group.posts.where(regular_removing: true).where(text: self.text).any?

    previous_posts = self.group.posts.where.not(vk_id: self.vk_id)
                         .where(from_vk_id: self.from_vk_id, is_deleted: false, text: self.text).all
    previous_posts.each do |t|
      begin
        t.destroy_object
      rescue
        t.is_deleted = true
        t.save
      end
    end
  end

end