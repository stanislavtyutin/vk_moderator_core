module ScheduledPublication

  def publish_in_future
    return unless self.post_type == 'suggest'

    return unless self.group.city

    unless self.check_result
      reasons = self.get_removing_reason
      reasons.delete :in_white_list
      reasons.delete :membership
      send_not_member_reject_message unless reasons.values.include?(false)
      return false
    end

    return false if friends_count < GlobalConfig.min_friends

    timetable_item = self.group.get_nearest_timetable_item
    if timetable_item
      unless RepostChecker.check self.group, self.from_vk_id
        return false
      end
      timetable_item.post = self
      timetable_item.publish_date = timetable_item.get_publish_time
      self.publish_suggested timetable_item.publish_date, true
      timetable_item.save
      true
    else
      false
    end
  end

  def check_last_post
    not self.group.posts.where.not(id: self.id).where(from_vk_id: user_vk_id).any?
  end

  def send_not_member_reject_message
    return unless check_last_post

    msg = MessagesTemplate.process self.group.post_request_messages_template, self
    zonbibare_api = ZonbibareApi.new
    zonbibare_api.create_send_message_task self.from_vk_id, msg
  end

  private

  def friends_count
    driver = VkApi.new self.group.select_bot
    data = driver.run_method 'friends.get', user_id: self.from_vk_id, fields: 'city'
    total = data['response']['count']
    in_city = data['response']['items'].select { |t| t['city'] == self.group.city }.count

    if in_city.to_f / total.to_f > 0.3
      total
    else
      0
    end
  end

end