class RepostChecker

  def self.check(group, user_vk_id)
    driver = VkApi.new group.select_bot
    last_posts = driver.run_method('wall.get', owner_id: user_vk_id, count: 10)['response']['items']

    last_posts.each do |post|
      post['copy_history']&.each do |t|
        return true if t['owner_id'] == -group.vk_id
      end

      att_data = post['attachments'].first
      if post['text'].empty? && att_data && att_data['type'] == 'link' && att_data['link']
        short_id = att_data['link']['url'].split('/').last
        id = driver.run_method('groups.getById', group_ids: short_id)['response'][0]['id'].to_i
        return true if group.vk_id == id
      end
    end

    false
  rescue
    false
  end

end