class InvalidSerial < RuntimeError
end

class Unauthorized < RuntimeError
end

class InvalidToken < RuntimeError
end

class InvalidIp < RuntimeError
end

class VkApiError < RuntimeError
end

class InvalidMethodError < VkApiError
end

class AccessDeniedError < VkApiError
end

class NoBotError < VkApiError
end

class AlreadyDeleted < RuntimeError
end