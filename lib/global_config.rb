class GlobalConfig

  GLOBAL_BIN_PATH = 'config/global.bin'.freeze

  def self.config_field(var_name)
    @fields ||= []
    @fields << var_name

    define_singleton_method var_name do
      dump = RedisProvider.instance.redis.get var_name
      begin
        Marshal.load dump
      rescue
        nil
      end
    end

    define_singleton_method "#{var_name}=" do |value|
      dump = Marshal.dump value
      RedisProvider.instance.redis.set var_name, dump
      value
    end
  end

  config_field :stop_words
  config_field :post_timeout
  config_field :old_timeout
  config_field :min_followers
  config_field :min_friends
  config_field :min_invited_friends
  config_field :request_invite_count
  config_field :max_timetable_offset

  def self.raw_config
    config = {}
    @fields.each do |t|
      config[t] = GlobalConfig.public_send t
    end
    config
  end

  def self.raw_config=(value)
    value.each do |k, v|
      GlobalConfig.public_send "#{k}=", v
    end
    value
  end

  private

  def self.get_config
    Marshal.load(File.read(GLOBAL_BIN_PATH))
  end

  def self.save_config(config)
    File.write GLOBAL_BIN_PATH, Marshal.dump(config)
  end

end