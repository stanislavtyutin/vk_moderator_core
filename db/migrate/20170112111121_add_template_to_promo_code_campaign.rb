class AddTemplateToPromoCodeCampaign < ActiveRecord::Migration
  def change
    add_column :promo_code_campaigns, :template, :text, null: false, default: '[promo_code]'
  end
end
