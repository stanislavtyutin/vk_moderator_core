class CreateTimetableItems < ActiveRecord::Migration
  def change
    create_table :timetable_items do |t|
      t.belongs_to :group, null: false
      t.belongs_to :post

      t.boolean :reserved, null: false, default: false

      t.integer :hour, null: false
      t.integer :min, null: false

      t.datetime :publish_date
    end
  end
end
