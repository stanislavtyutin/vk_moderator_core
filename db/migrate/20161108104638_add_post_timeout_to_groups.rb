class AddPostTimeoutToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :post_timeout, :integer
  end
end
