class CreateWhiteUsers < ActiveRecord::Migration
  def change
    create_table :white_users do |t|
      t.belongs_to :group, null: false

      t.string :vk_id, null: false
      t.string :name

      t.datetime :start_date, null: false
      t.datetime :stop_date, null: false

      t.timestamps null: false
    end
  end
end
