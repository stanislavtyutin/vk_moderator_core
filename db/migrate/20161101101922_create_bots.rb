class CreateBots < ActiveRecord::Migration
  def change
    create_table :bots do |t|
      t.integer :vk_id, null: false
      t.string :access_token, null: false
      t.boolean :active, null: false, default: true
      t.string :name, null: false

      t.timestamps null: false
    end
  end
end
