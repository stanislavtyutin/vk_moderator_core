class AddOldTimeoutToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :old_timeout, :integer
  end
end
