class AddEnableMessageOnRejectToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :enable_message_on_reject, :boolean, null: false, default: false
  end
end
