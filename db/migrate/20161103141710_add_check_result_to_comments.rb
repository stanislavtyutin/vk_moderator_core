class AddCheckResultToComments < ActiveRecord::Migration
  def change
    add_column :comments, :check_result, :boolean, null: false, default: true
  end
end
