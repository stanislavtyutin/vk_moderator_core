class AddCityToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :city, :integer, null: true
  end
end
