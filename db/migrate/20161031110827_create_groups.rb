class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|

      t.integer :vk_id, null: false
      t.string :name
      t.string :passthrough_url
      t.string :stop_words, null: false, default: ''

    end
  end
end
