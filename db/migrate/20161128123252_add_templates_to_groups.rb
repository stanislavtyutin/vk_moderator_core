class AddTemplatesToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :post_request_messages_template, :text
    add_column :groups, :post_reject_messages_template, :text
  end
end
