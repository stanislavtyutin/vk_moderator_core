class CreatePostRequests < ActiveRecord::Migration
  def change
    create_table :post_requests do |t|
      t.belongs_to :post, null: false
      t.boolean :active, null: false, default: true
      t.boolean :posted, null: false, default: false

      t.string :request_type, null: false
      t.text :data

      t.timestamps null: false
    end
  end
end
