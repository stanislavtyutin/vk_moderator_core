class CreateBotsGroups < ActiveRecord::Migration
  def change
    create_table :bots_groups, id: false do |t|
      t.references :bot, null: false
      t.references :group, null: false
    end

    add_index :bots_groups, [:bot_id, :group_id], unique: true
  end
end
