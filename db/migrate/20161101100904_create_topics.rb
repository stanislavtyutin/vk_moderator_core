class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.belongs_to :group, null: false

      t.integer :vk_id, null: false
      t.string :title, null: false
      t.datetime :publish_date, null: false
      t.boolean :is_closed, null: false, default: false
      t.boolean :is_fixed, null: false, default: false
    end
  end
end
