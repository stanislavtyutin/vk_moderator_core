class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :subject_type, null: false
      t.integer :subject_id, null: false

      t.integer :vk_id, null: false
      t.integer :from_vk_id, null: false
      t.datetime :publish_date, null: false
      t.text :text, null: false, default: ''
    end

    add_index :comments, [:subject_type, :subject_id]
  end
end
