class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :subject_type, null: false
      t.integer :subject_id, null: false

      t.string :attachment_type, null: false
      t.string :data, null: false, default: ''
      t.string :full_data, null: false, default: ''
    end

    add_index :attachments, [:subject_type, :subject_id]
  end
end
