class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.belongs_to :group, null: false, index: true

      t.integer :vk_id, null: false
      t.integer :from_vk_id, null: false
      t.datetime :publish_date, null: false
      t.string :post_type, null: false
      t.text :text, null: false, default: ''

      t.boolean :check_result, null: false, default: true
    end
  end
end
