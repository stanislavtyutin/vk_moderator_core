class AddMinFriendsToPromoCodesCampaign < ActiveRecord::Migration
  def change
    add_column :promo_code_campaigns, :min_friends_count, :integer, null: false, default: 0
    add_column :promo_code_campaigns, :min_friends_fail_message, :string
  end
end
