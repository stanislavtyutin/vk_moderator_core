class CreateWhiteLinks < ActiveRecord::Migration
  def change
    create_table :white_links do |t|
      t.belongs_to :group, null: false

      t.string :link, null: false

      t.datetime :start_date, null: false
      t.datetime :stop_date, null: false

      t.timestamps null: false
    end
  end
end
