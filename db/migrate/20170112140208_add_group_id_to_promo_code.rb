class AddGroupIdToPromoCode < ActiveRecord::Migration
  def change
    add_column :promo_codes, :group_id, :integer
  end
end
