class AddMaxTimetableOffsetToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :max_timetable_offset, :integer
  end
end
