class AddActiveToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :active, :boolean, null: false, default: false
    add_column :groups, :vk_secret, :string
  end
end
