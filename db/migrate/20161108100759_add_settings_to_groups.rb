class AddSettingsToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :enable_check_wall, :boolean, null: false, default: false
    add_column :groups, :enable_check_topics, :boolean, null: false, default: false
    add_column :groups, :enable_check_photos, :boolean, null: false, default: false
    add_column :groups, :enable_check_videos, :boolean, null: false, default: false
    add_column :groups, :enable_check_market_items, :boolean, null: false, default: false

    add_column :groups, :enable_check_stop_words, :boolean, null: false, default: false
    add_column :groups, :enable_check_links, :boolean, null: false, default: false
    add_column :groups, :enable_check_text_exists, :boolean, null: false, default: false
    add_column :groups, :enable_check_membership, :boolean, null: false, default: false
    add_column :groups, :enable_check_timeout, :boolean, null: false, default: false

    add_column :groups, :enable_remove_old_posts, :boolean, null: false, default: false
    add_column :groups, :enable_remove_old_topic_comments, :boolean, null: false, default: false

    add_column :groups, :enable_auto_publish_suggested, :boolean, null: false, default: false
    add_column :groups, :enable_suggest_publish_from_group, :boolean, null: false, default: false
  end
end
