class CreateDialogStructures < ActiveRecord::Migration
  def change
    create_table :dialog_structures do |t|
      t.belongs_to :parent
      t.string :key
      t.string :back_key
      t.string :reset_key
      t.text :text, null: false
    end

    add_column :groups, :enable_auto_answering, :boolean, null: false, default: false
    add_column :groups, :dialog_structure_id, :integer
  end
end
