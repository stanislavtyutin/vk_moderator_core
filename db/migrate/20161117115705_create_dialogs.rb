class CreateDialogs < ActiveRecord::Migration
  def change
    create_table :dialogs do |t|
      t.belongs_to :group, null: false
      t.belongs_to :dialog_structure, null: false
      t.integer :user_vk_id, null: false

      t.timestamps null: false
    end
  end
end
