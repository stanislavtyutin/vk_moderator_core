class AddAccessTokenToGroup < ActiveRecord::Migration
  def change
    add_column :groups, :access_token, :string
  end
end
