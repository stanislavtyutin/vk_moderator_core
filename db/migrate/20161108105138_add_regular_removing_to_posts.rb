class AddRegularRemovingToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :regular_removing, :boolean, null: false, default: false
  end
end
