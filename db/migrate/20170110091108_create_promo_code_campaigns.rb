class CreatePromoCodeCampaigns < ActiveRecord::Migration
  def change
    create_table :promo_code_campaigns do |t|
      t.string :name
      t.string :key, null: false
      t.boolean :active, null: false, default: true
    end
  end
end
