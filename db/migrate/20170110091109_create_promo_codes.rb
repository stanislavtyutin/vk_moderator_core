class CreatePromoCodes < ActiveRecord::Migration
  def change
    create_table :promo_codes do |t|
      t.belongs_to :promo_code_campaign, null: false
      t.string :code, null: false
      t.integer :user_vk_id
      t.timestamps null: false
    end
  end
end
