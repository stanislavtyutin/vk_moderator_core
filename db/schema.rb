# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170125094417) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "attachments", force: :cascade do |t|
    t.string  "subject_type",                 null: false
    t.integer "subject_id",                   null: false
    t.string  "attachment_type",              null: false
    t.string  "data",            default: "", null: false
    t.string  "full_data",       default: "", null: false
  end

  add_index "attachments", ["subject_type", "subject_id"], name: "index_attachments_on_subject_type_and_subject_id"

  create_table "bots", force: :cascade do |t|
    t.integer  "vk_id",                       null: false
    t.string   "access_token",                null: false
    t.boolean  "active",       default: true, null: false
    t.string   "name",                        null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "bots_groups", id: false, force: :cascade do |t|
    t.integer "bot_id",   null: false
    t.integer "group_id", null: false
  end

  add_index "bots_groups", ["bot_id", "group_id"], name: "index_bots_groups_on_bot_id_and_group_id", unique: true

  create_table "comments", force: :cascade do |t|
    t.string   "subject_type",                 null: false
    t.integer  "subject_id",                   null: false
    t.integer  "vk_id",                        null: false
    t.integer  "from_vk_id",                   null: false
    t.datetime "publish_date",                 null: false
    t.text     "text",         default: "",    null: false
    t.boolean  "is_deleted",   default: false, null: false
    t.boolean  "check_result", default: true,  null: false
  end

  add_index "comments", ["subject_type", "subject_id"], name: "index_comments_on_subject_type_and_subject_id"

  create_table "dialog_structures", force: :cascade do |t|
    t.integer "parent_id"
    t.string  "key"
    t.string  "back_key"
    t.string  "reset_key"
    t.text    "text",      null: false
  end

  create_table "dialogs", force: :cascade do |t|
    t.integer  "group_id",            null: false
    t.integer  "dialog_structure_id", null: false
    t.integer  "user_vk_id",          null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "groups", force: :cascade do |t|
    t.integer "vk_id",                                             null: false
    t.string  "name"
    t.string  "passthrough_url"
    t.string  "stop_words",                        default: "",    null: false
    t.string  "vk_code"
    t.boolean "active",                            default: false, null: false
    t.string  "vk_secret"
    t.boolean "enable_check_wall",                 default: false, null: false
    t.boolean "enable_check_topics",               default: false, null: false
    t.boolean "enable_check_photos",               default: false, null: false
    t.boolean "enable_check_videos",               default: false, null: false
    t.boolean "enable_check_market_items",         default: false, null: false
    t.boolean "enable_check_stop_words",           default: false, null: false
    t.boolean "enable_check_links",                default: false, null: false
    t.boolean "enable_check_text_exists",          default: false, null: false
    t.boolean "enable_check_membership",           default: false, null: false
    t.boolean "enable_check_timeout",              default: false, null: false
    t.boolean "enable_remove_old_posts",           default: false, null: false
    t.boolean "enable_remove_old_topic_comments",  default: false, null: false
    t.boolean "enable_auto_publish_suggested",     default: false, null: false
    t.boolean "enable_suggest_publish_from_group", default: false, null: false
    t.integer "post_timeout"
    t.integer "old_timeout"
    t.string  "access_token"
    t.boolean "enable_auto_answering",             default: false, null: false
    t.integer "dialog_structure_id"
    t.integer "max_timetable_offset"
    t.text    "post_request_messages_template"
    t.text    "post_reject_messages_template"
    t.boolean "enable_message_on_reject",          default: false, null: false
    t.integer "city"
  end

  create_table "post_requests", force: :cascade do |t|
    t.integer  "post_id",                      null: false
    t.boolean  "active",       default: true,  null: false
    t.boolean  "posted",       default: false, null: false
    t.string   "request_type",                 null: false
    t.text     "data"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "group_id",                         null: false
    t.integer  "vk_id",                            null: false
    t.integer  "from_vk_id",                       null: false
    t.datetime "publish_date",                     null: false
    t.string   "post_type",                        null: false
    t.text     "text",             default: "",    null: false
    t.boolean  "check_result",     default: true,  null: false
    t.boolean  "is_deleted",       default: false, null: false
    t.boolean  "regular_removing", default: false, null: false
  end

  add_index "posts", ["group_id"], name: "index_posts_on_group_id"

  create_table "promo_code_campaigns", force: :cascade do |t|
    t.string  "name"
    t.string  "key",                                               null: false
    t.boolean "active",                   default: true,           null: false
    t.text    "template",                 default: "[promo_code]", null: false
    t.integer "min_friends_count",        default: 0,              null: false
    t.string  "min_friends_fail_message"
  end

  create_table "promo_codes", force: :cascade do |t|
    t.integer  "promo_code_campaign_id", null: false
    t.string   "code",                   null: false
    t.integer  "user_vk_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "group_id"
  end

  create_table "timetable_items", force: :cascade do |t|
    t.integer  "group_id",                     null: false
    t.integer  "post_id"
    t.boolean  "reserved",     default: false, null: false
    t.integer  "hour",                         null: false
    t.integer  "min",                          null: false
    t.datetime "publish_date"
  end

  create_table "topics", force: :cascade do |t|
    t.integer  "group_id",                     null: false
    t.integer  "vk_id",                        null: false
    t.string   "title",                        null: false
    t.datetime "publish_date",                 null: false
    t.boolean  "is_closed",    default: false, null: false
    t.boolean  "is_fixed",     default: false, null: false
  end

  create_table "white_links", force: :cascade do |t|
    t.integer  "group_id",   null: false
    t.string   "link",       null: false
    t.datetime "start_date", null: false
    t.datetime "stop_date",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "white_users", force: :cascade do |t|
    t.integer  "group_id",   null: false
    t.string   "vk_id",      null: false
    t.string   "name"
    t.datetime "start_date", null: false
    t.datetime "stop_date",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
