# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'vk_moderator_code'
set :repo_url, 'ssh://gitlab@git.tbrain.ru:2222/stanislav.tyutin/vk_moderator_core.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/opt/deploy/vk_moderator_core'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, 'config/database.yml', 'config/secrets.yml'
append :linked_files, 'unicorn.rb'

# Default value for linked_dirs is []
append :linked_dirs, 'config', 'log', 'tmp'

# Default value for default_env is {}
set :default_env, { 'RACK_ENV' => 'production' }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Bundler config
#set :bundle_jobs, 4

# Start-up
set :pid, -> { File.join(current_path, 'tmp', 'pids', 'unicorn.pid') }
