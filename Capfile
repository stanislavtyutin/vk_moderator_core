# Load DSL and set up stages
require "capistrano/setup"

# Include default deployment tasks
require "capistrano/deploy"

# Include tasks from other gems included in your Gemfile
#
# For documentation on these, see for example:
#
#   https://github.com/capistrano/rvm
#   https://github.com/capistrano/rbenv
#   https://github.com/capistrano/chruby
#   https://github.com/capistrano/bundler
#   https://github.com/capistrano/rails
#   https://github.com/capistrano/passenger
#
# require 'capistrano/rvm'
# require 'capistrano/rbenv'
# require 'capistrano/chruby'
require 'capistrano/bundler'
# require 'capistrano/rails/assets'
# require 'capistrano/rails/migrations'
# require 'capistrano/passenger'

# Load custom tasks from `lib/capistrano/tasks` if you have any defined
Dir.glob("lib/capistrano/tasks/*.rake").each { |r| import r }

namespace :deploy do
  task :migrate do
    on roles(:app) do
      within "#{current_path}" do
        execute :rake, 'db:migrate'
      end
    end
  end

  task :start do
    on roles(:app) do
      within "#{current_path}" do
        if test("[ -e #{fetch(:pid)} ] && kill -0 #{pid}")
          info 'Running'
        else
          execute "cd #{current_path} && (RACK_ENV=production unicorn -D -c unicorn.rb)"
        end
      end
    end
  end

  task :stop do
    on roles(:app) do
      within "#{current_path}" do
        if test("[ -e #{fetch(:pid)} ] && kill -0 #{pid}")
          execute :kill, pid
        else
          info 'Stopped'
        end
      end
    end
  end

  task :restart do
    invoke 'deploy:stop'
    sleep 5
    invoke 'deploy:start'
  end

  def pid
    "`cat #{fetch(:pid)}`"
  end

  after 'deploy', 'deploy:migrate'
  after 'deploy:migrate', 'deploy:restart'


end
