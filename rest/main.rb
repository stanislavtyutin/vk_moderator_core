require 'sinatra/base'
require 'sinatra/json'
require 'sinatra/reloader'

require_all 'rest/**/*.rb'

class VkModeratorCore < Sinatra::Base
  register Sinatra::ActiveRecordExtension

  use SystemRest
  use SessionRest
  use VkCallbackRest
  use ApiRest

  configure :development do
    register Sinatra::Reloader
  end

  get '/' do
    'VkModeratorCore'
  end

  not_found do
    halt 404, 'Not found'
  end

  def self.debug_server
    run!
  end
end
