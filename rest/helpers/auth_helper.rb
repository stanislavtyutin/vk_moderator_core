module AuthHelper

  def auth!
    unless params[:token]
      raise Unauthorized
    end
    SessionManager.auth! params[:token]
    nil
  rescue Unauthorized
    halt 403, 'Token invalid'
  end

  def current_instance
    unless params[:token]
      raise Unauthorized
    end
    session = SessionManager.get_session params[:token]
    session.instance
  rescue Unauthorized
    halt 403, 'Token invalid'
  end

  def current_session
    unless params[:token]
      raise Unauthorized
    end
    SessionManager.get_session params[:token]
  rescue Unauthorized
    halt 403, 'Token invalid'
  end

  def ip
    request.env['X-REAL-IP'] || request.env['REMOTE_ADDR'] || '?'
  end

end