module Passthrough
  require 'net/http'
  require 'uri'

  def passthrough_request(data)
    group = Group.find_by_vk_id data['group_id']
    if group&.passthrough_url
      begin
        uri = URI.parse group.passthrough_url
        http = Net::HTTP.new uri.host, uri.port
        request = Net::HTTP::Post.new uri.request_uri
        request.body = data.to_json
        http.request request
      rescue
        nil
      end
    end
  end

end