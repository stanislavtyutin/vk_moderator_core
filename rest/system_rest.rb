class SystemRest < Sinatra::Base

  MODULE_PATH ||= '/system'

  configure :development do
    register Sinatra::Reloader
  end

  get "#{MODULE_PATH}/datetime" do
    json current_time: Time.now.to_i
  end

  get "#{MODULE_PATH}/datetime.txt" do
    Time.now.to_i.to_s
  end

end