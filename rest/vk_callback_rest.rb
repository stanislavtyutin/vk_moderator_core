class VkCallbackRest < Sinatra::Base

  MODULE_PATH ||= '/callback'

  configure :development do
    register Sinatra::Reloader
  end

  helpers Passthrough

  post MODULE_PATH do
    begin
      result = nil

      data = JSON.parse request.body.read

      passthrough_request data

      if data['type'] == 'confirmation'
        group = Group.find_by_vk_id(data['group_id'].to_i)
        puts group.vk_code
        result = group&.vk_code&.to_s
      else
        RequestProcessor.process data
      end
    rescue Exception => e
      puts e
      puts e.backtrace
    ensure
      status 200
      body result.kind_of?(String) ? result : 'ok'
      return nil
    end
  end

end