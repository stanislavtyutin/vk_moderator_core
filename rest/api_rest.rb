class ApiRest < Sinatra::Base

  MODULE_PATH ||= '/api'

  configure :development do
    register Sinatra::Reloader
  end

  get MODULE_PATH do
    'api'
  end

  get "#{MODULE_PATH}/post/:id/removing_reason" do
    post = Post.find params[:id]
    post.get_removing_reason.to_json
  end

  get "#{MODULE_PATH}/comment/:id/removing_reason" do
    comment = Comment.find params[:id]
    comment.get_removing_reason.to_json
  end

  get "#{MODULE_PATH}/bot/:id/update" do
    bot = Bot.find params[:id]
    bot.load_name
    bot.save
    'ok'
  end

  get "#{MODULE_PATH}/bot/:id/load_groups" do
    bot = Bot.find params[:id]
    bot.load_groups
    'ok'
  end

  get "#{MODULE_PATH}/group/:id/update" do
    group = Group.find params[:id]
    group.load_topics
    'ok'
  end

  get "#{MODULE_PATH}/white_user/:id/update" do
    white_user = WhiteUser.find params[:id]
    white_user.load_name
    white_user.save
    'ok'
  end

  get "#{MODULE_PATH}/config" do
    GlobalConfig.raw_config.to_json
  end

  post "#{MODULE_PATH}/config" do
    source = JSON.parse request.body.read
    config = source.inject({}) { |memo, (k, v)| memo[k.to_sym] = v; memo }
    GlobalConfig.raw_config = config
    'ok'
  end

  get "#{MODULE_PATH}/app_install_link" do
    VkApi.app_install_link
  end

  get "#{MODULE_PATH}/app_group_install_link" do
    VkApi.app_group_install_link params[:vk_id]
  end

end