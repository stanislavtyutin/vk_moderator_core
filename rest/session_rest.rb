class SessionRest < Sinatra::Base

  MODULE_PATH = '/session'

  configure :development do
    register Sinatra::Reloader
  end

  helpers AuthHelper

  #get session info
  # get MODULE_PATH do
  #   token = params[:token]
  #   halt 400 unless token
  #   begin
  #     session = SessionManager.get_session token
  #     session.token
  #   rescue InvalidToken, TokenExpired
  #     halt 403
  #   rescue
  #     halt 500
  #   end
  # end

  #create new session
  put MODULE_PATH do
    serial = params[:serial]

    begin
      session = SessionManager.create_session serial, ip
      session.token
    rescue InvalidSerial
      halt 403
    rescue
      halt 500
    end

  end

  #destroy session
  # delete MODULE_PATH do
  #   token = params[:token]
  #   halt 400 unless token
  #   begin
  #     SessionManager.destroy_session token
  #     'ok'
  #   rescue
  #     halt 500
  #   end
  # end

  #modify expire date
  # post MODULE_PATH do
  #   token = params[:token]
  #   timeout = params[:timeout]
  #   halt 400 unless token && timeout
  #   begin
  #     session = SessionManager.get_session token
  #     session.timeout = timeout.to_i.seconds
  #     SessionManager.save_session session
  #     json session.as_json
  #   rescue InvalidToken, TokenExpired
  #     status 403
  #     json error: 'Token invalid'
  #   rescue
  #     halt 500
  #   end
  # end

end